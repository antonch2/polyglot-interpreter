package org.appmat.polyglot

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.*
import io.ktor.server.netty.*
import kotlinx.coroutines.Dispatchers
import org.appmat.polyglot.notebook.repository.NotebookTable
import org.appmat.polyglot.user.repository.UserTable
import org.appmat.polyglot.notebook.notebookModule
import org.appmat.polyglot.notebook.paragraph.paragraphModule
import org.appmat.polyglot.notebook.paragraph.repository.ParagraphTable
import org.appmat.polyglot.plugins.cors.configureCORS
import org.appmat.polyglot.plugins.monitoring.configureMonitoring
import org.appmat.polyglot.plugins.di.configureDI
import org.appmat.polyglot.plugins.healthcheck.configureHealthChecks
import org.appmat.polyglot.plugins.authentication.configureAuth
import org.appmat.polyglot.plugins.routing.configureRouting
import org.appmat.polyglot.plugins.serialization.configureSerialization
import org.appmat.polyglot.user.userModule
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.module.Module

fun main(args: Array<String>): Unit = EngineMain.main(args)

object DatabaseFactory {
    fun init(environment: ApplicationEnvironment) {
        val configPath = environment.config.property("ktor.dbconfig").getString()
        val dbConfig = HikariConfig(configPath)
        val dataSource = HikariDataSource(dbConfig)
        val database = Database.connect(dataSource)
        transaction(database) {
            SchemaUtils.create(UserTable)
            SchemaUtils.create(NotebookTable)
            SchemaUtils.create(ParagraphTable)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }
}


@Suppress("unused")
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false,
                       koinModules: List<Module> = listOf(userModule, notebookModule, paragraphModule)) {

    val jwtConfig = environment.config.config("ktor.jwt")
    DatabaseFactory.init(environment)

    configureDI(koinModules)
    configureAuth(jwtConfig)
    configureRouting()
    configureMonitoring()
    configureSerialization()
    configureHealthChecks()

    configureCORS(environment.config.config("ktor.cors").property("hosts").getList())
}

fun Application.provideApplicationEnvironment(): ApplicationEnvironment = environment

