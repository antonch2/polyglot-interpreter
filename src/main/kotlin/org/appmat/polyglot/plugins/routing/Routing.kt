package org.appmat.polyglot.plugins.routing

import io.ktor.application.*
import io.ktor.locations.*
import io.ktor.routing.*
import org.koin.ktor.ext.getKoin

fun Application.configureRouting() {
    install(Locations)

    routing {
        getKoin().getAll<KtorController>()
            .distinct()
            .forEach { it.routing(this) }
    }
}
