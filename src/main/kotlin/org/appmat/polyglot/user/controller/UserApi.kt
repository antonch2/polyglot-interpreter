package org.appmat.polyglot.user.controller

import io.ktor.locations.*

@Location("/api")
class Api {
    @Location("/user")
    class User(val api: Api) {
        @Location("/register")
        class Register(val user: User)
        @Location("/unregister")
        class Unregister(val user: User, val uid: Long)
    }
}