package org.appmat.polyglot.user.controller

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.locations.post
import io.ktor.routing.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.appmat.polyglot.plugins.routing.KtorController
import org.appmat.polyglot.user.dto.KeycloakUserRequest
import org.appmat.polyglot.user.UserService
import org.appmat.polyglot.user.controller.UserMapper.toUser
import java.util.*

class UserController(
    private val userService: UserService,
    private val env: ApplicationEnvironment,
) : KtorController {
    private val conf = env.config.config("ktor.jwt")
    private val secret = conf.property("secret").getString()
    private val issuer = conf.property("issuer").getString()
    private val audience = conf.property("audience").getString()

    override val routing: Routing.() -> Unit = {
        post<Api.User.Register> {
            val currentUser = call.receive<KeycloakUserRequest>().toUser()
            val user = userService.getUser(currentUser.idToken.toLong())
            if (user != null && user.authenticated) {
                val token = JWT.create()
                    .withAudience(audience)
                    .withIssuer(issuer)
                    .withClaim("token", currentUser.idToken)
                    .withExpiresAt(Date(System.currentTimeMillis() + 24 * 60 * 60000))
                    .sign(Algorithm.HMAC256(secret))
                call.respondText(
                    Json.encodeToString(hashMapOf("token" to token)),
                    ContentType.Application.Json
                )
            } else {
                call.response.status(HttpStatusCode.Unauthorized)
                userService.registerUser(currentUser)
                call.respondText(
                    Json.encodeToString(hashMapOf("error" to "Wrong login or password")),
                    ContentType.Application.Json
                )
            }
            call.respond(HttpStatusCode.OK)
        }

        post<Api.User.Unregister> {
            val currentUser = call.receive<KeycloakUserRequest>().toUser()
            val user = userService.getUser(currentUser.idToken.toLong())
            if (user != null && user.authenticated) {
                val token = JWT.create()
                    .withAudience(audience)
                    .withIssuer(issuer)
                    .withClaim("token", currentUser.idToken)
                    .withExpiresAt(Date(System.currentTimeMillis() + 24 * 60 * 60000))
                    .sign(Algorithm.HMAC256(secret))
                call.respondText(
                    Json.encodeToString(hashMapOf("token" to token)),
                    ContentType.Application.Json
                )
            } else {
                call.response.status(HttpStatusCode.Unauthorized)
                userService.unRegisterUser(currentUser)
                call.respondText(
                    Json.encodeToString(hashMapOf("error" to "Wrong login or password")),
                    ContentType.Application.Json
                )
            }
            call.respond(HttpStatusCode.OK)
        }
    }

}