package org.appmat.polyglot.notebook.controller

import io.ktor.application.*

object NotebookUtils {
    fun ApplicationCall.receiveNotebookId(): String = request.call.parameters["notebookId"]!!

    fun ApplicationCall.receiveNotebookAndParagraphIds(): Pair<String, String> {
        return Pair(request.call.parameters["notebookId"]!!, request.call.parameters["paragraphId"]!!)
    }

    fun ApplicationCall.receiveAuthToken(): String? = request.headers["Authorization"]
}