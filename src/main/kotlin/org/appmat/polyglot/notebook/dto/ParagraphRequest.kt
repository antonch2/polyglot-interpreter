package org.appmat.polyglot.notebook.dto

import kotlinx.serialization.Required
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ParagraphRequest(
    @SerialName(value = "language") @Required val language: String,
    @SerialName(value = "code") @Required val code: String
)