package org.appmat.polyglot.notebook.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PolyglotVariableDto(
    @SerialName(value = "name") var name: String,
    @SerialName(value = "type") var type: String? = "",
    @SerialName(value = "value") var value: String? = ""
)