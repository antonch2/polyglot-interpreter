package org.appmat.polyglot.notebook.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ParagraphDto(
    @SerialName(value = "id") val id: Int,
    @SerialName(value = "notebook_id") val notebookId: Int,
    @SerialName(value = "language") val language: String,
    @SerialName(value = "code") val code: String,
    @SerialName(value = "output") val output: String,
)