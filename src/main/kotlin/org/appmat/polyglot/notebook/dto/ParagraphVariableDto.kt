package org.appmat.polyglot.notebook.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ParagraphVariableDto(
    @SerialName("name") val name: String,
    @SerialName("value") val value: String,
    @SerialName("type") val type: String,
    @SerialName("metadata") val metadata: String
)