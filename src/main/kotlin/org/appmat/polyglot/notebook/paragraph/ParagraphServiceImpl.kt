package org.appmat.polyglot.notebook.paragraph

import org.appmat.polyglot.notebook.paragraph.repository.ParagraphRepository

class ParagraphServiceImpl: ParagraphService {

    private val repository: ParagraphRepository = ParagraphRepository()

    override fun createParagraph(paragraph: Paragraph): Int {
        return repository.createParagraph(paragraph)
    }

    override fun deleteParagraph(notebookId: Int, paragraphId: Int): Int {
        return repository.deleteParagraph(notebookId, paragraphId)
    }
}