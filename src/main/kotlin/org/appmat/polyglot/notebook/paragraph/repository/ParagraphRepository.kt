package org.appmat.polyglot.notebook.paragraph.repository

import org.appmat.polyglot.notebook.paragraph.Paragraph
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class ParagraphRepository {

    fun getAllParagraphs(notebookId: Int):ArrayList<Paragraph>{
        val paragraphs: ArrayList<Paragraph> = arrayListOf()
        transaction {
            ParagraphTable.select { ParagraphTable.notebookId eq notebookId }.map{
                paragraphs.add(
                    Paragraph(
                        id = it[ParagraphTable.paragraphId],
                        notebookId,
                        language = it[ParagraphTable.language],
                        code = it[ParagraphTable.code],
                        output = it[ParagraphTable.output]
                    )
                )
            }
        }
        return paragraphs;
    }

    private fun getAllParagraphsId(notebookId: Int):ArrayList<Int>{
        val paragraphsId: ArrayList<Int> = arrayListOf()
        transaction {
            ParagraphTable.select { ParagraphTable.notebookId eq notebookId }.map{
                paragraphsId.add(
                        it[ParagraphTable.paragraphId]
                    )
            }
        }
        return paragraphsId;
    }

    fun updateParagraph(paragraph: Paragraph){
        transaction {
            ParagraphTable.update ({ ParagraphTable.notebookId eq paragraph.notebookId}) {
                it[language] = paragraph.language
                it[code] = paragraph.code
                it[output] = paragraph.output
            }
        }
    }

    fun createParagraph(paragraph: Paragraph): Int {
        val listIds = getAllParagraphsId(paragraph.notebookId)
        var index = listIds.maxOrNull()
        if (index == null) {
            index = 0
        } else{
            index.plus(1)
        }
        transaction {
            ParagraphTable.insert {
                it[id] = index
                it[language] = paragraph.language
                it[code] = paragraph.code
                it[output] = paragraph.output
            }
        }
        return index
    }

    fun deleteParagraph(notebookId: Int, paragraphId: Int): Int {
        transaction {
            ParagraphTable.deleteWhere {(ParagraphTable.notebookId eq notebookId) and (ParagraphTable.paragraphId eq paragraphId)}
        }
        return paragraphId
    }

}