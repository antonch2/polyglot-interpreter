package org.appmat.polyglot.notebook.repository

import org.appmat.polyglot.notebook.Notebook
import org.appmat.polyglot.notebook.paragraph.repository.ParagraphRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate
import kotlin.collections.ArrayList

class NotebookRepository {

    val repository: ParagraphRepository = ParagraphRepository()

    fun getAllNotebooks(userId: Int): ArrayList<Notebook> {
        val notebooks: ArrayList<Notebook> = arrayListOf()
        transaction {
            NotebookTable.select { NotebookTable.userId eq userId }.map {
                notebooks.add(
                    Notebook(
                        id = it[NotebookTable.id],
                        title = it[NotebookTable.title],
                        date = it[NotebookTable.date],
                        paragraphs = repository.getAllParagraphs(it[NotebookTable.notebookId])
                    )
                )
            }
        }
        return notebooks
    }

    fun getNotebookById(userId: Int, notebookId: Int): Notebook {
        var notebook = Notebook(notebookId, "", "")
        transaction {
            NotebookTable.select { (NotebookTable.userId eq userId) and (NotebookTable.notebookId eq notebookId) }.map {
                notebook = Notebook(
                    id = it[NotebookTable.id],
                    title = it[NotebookTable.title],
                    date = it[NotebookTable.date],
                    paragraphs = repository.getAllParagraphs(it[NotebookTable.notebookId])
                )
            }
        }
        return notebook
    }

    fun createNotebook(userId: Int, title: String): Notebook {
        val date = LocalDate.now().toString()
        val notebookId = Sequence.nextValue()
        val notebook = Notebook(notebookId, title, date)
        transaction {
            NotebookTable.insert {
                it[id] = notebookId
                it[NotebookTable.userId] = userId
                it[NotebookTable.title] = title
                it[NotebookTable.date] = date
            }
        }
        return notebook
    }

    fun deleteNotebook(notebookId: Int) {
        transaction {
            NotebookTable.deleteWhere {NotebookTable.notebookId eq notebookId}
        }
    }
    fun updateNotebook(notebook: Notebook) {
        transaction {
            NotebookTable.update({NotebookTable.notebookId eq notebook.id}){
                it[title] = notebook.title
                it[date] = notebook.date
                notebook.paragraphs.forEach{paragraph -> repository.updateParagraph(paragraph)}
            }
        }
    }
}