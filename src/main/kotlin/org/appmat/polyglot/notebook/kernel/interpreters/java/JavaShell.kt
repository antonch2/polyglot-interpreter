package org.appmat.polyglot.notebook.kernel.interpreters.java

import jdk.jshell.JShell
import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.PolyglotType
import org.appmat.polyglot.notebook.kernel.PolyglotVariable
import org.appmat.polyglot.notebook.kernel.interpreters.Interpreters
import org.appmat.polyglot.notebook.kernel.interpreters.ShellConfiguration
import org.appmat.polyglot.notebook.kernel.interpreters.parseSimpleType
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class JavaJShell() : Interpreters {
    private lateinit var jsh: JShell
    private lateinit var variables: ArrayList<PolyglotVariable>

    override val language: String
        get() = "java"

    override fun initShell(config: ShellConfiguration?) {
        jsh = JShell.create()
        variables = arrayListOf<PolyglotVariable>()
    }

    override fun getVariables() : PolyglotContext {
        variables = arrayListOf<PolyglotVariable>()

        for (variable in jsh.variables()) {
            if (variable.name().contains("$")) {
                continue
            };
            val name = variable.name()
            var value = jsh.varValue(variable)
            val type = parseVariableType(variable.typeName())

            if (type is PolyglotType.ComplexType.SetType) {
                value = value.replace("[", "{").replace("]", "}")
            }
            variables.add(PolyglotVariable(name, value, type, variable.typeName(), null))
        }
        return PolyglotContext(variables, null, null)
    }

    override fun parseVariableType(type: String): PolyglotType? {
        val variableType = parseSimpleType(type)
            ?: with (type) {
                val varType = split("<")[0]
                val simpleMatcher = Pattern.compile("<(?<type>\\w+)>").matcher(this)
                val complexMatcher = Pattern.compile("<(?<key>\\w+), (?<value>\\w+)>").matcher(this)

                val t: PolyglotType? = when (varType) {
                    "HashSet" -> {
                        simpleMatcher.find()
                        val groupType = parseSimpleType(simpleMatcher.group("type"))
                        PolyglotType.ComplexType.SetType(type = groupType)
                    }
                    "ArrayList" -> {
                        simpleMatcher.find()
                        val groupType = parseSimpleType(simpleMatcher.group("type"))
                        PolyglotType.ComplexType.ListType(type = groupType)
                    }
                    "HashMap" -> {
                        complexMatcher.find()
                        val key = parseSimpleType(complexMatcher.group("key"))
                        val value = parseSimpleType(complexMatcher.group("value"))
                        PolyglotType.ComplexType.MapType(key = key, value = value)
                    }
                    else -> null
                }
                return t
            }
        return variableType
    }

    override fun execute(code: String) {
        var snippets: List<String> = code
            .split("\n\n")
            .map { it -> it.trimIndent()}
            .filter { it != "" }

        snippets.stream().forEach {snip ->
            if (snip.contains("Map") or snip.contains("Set") or snip.contains("Array")) {
                var l: String = "\n"
                val del = "\t\t\t\t\t"
                l += del + "import java.util.HashMap;\n"
                l += del + "import java.util.ArrayList;\n"
                l += del + "import java.util.HashSet;\n"

                snip.split("\n").filter { it != "" }.forEach {
                    l += del + it + "\n"
                }
                val snpts: List<String> = l.split("\n").map { it -> it.trimIndent() }
                snpts.stream().forEach {s ->
                    jsh.eval(s)
                }
            } else {
                jsh.eval(snip)
            }
        }
    }

    override fun reset() {
    }

    override fun shutdown() {
        jsh.close()
    }

}

