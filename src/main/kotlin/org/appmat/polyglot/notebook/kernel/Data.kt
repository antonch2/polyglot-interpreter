package org.appmat.polyglot.notebook.kernel

import org.appmat.polyglot.notebook.kernel.interpreters.java.JavaJShell
import org.appmat.polyglot.notebook.kernel.interpreters.javascript.JavascriptShell
import org.appmat.polyglot.notebook.kernel.interpreters.kotlin.KotlinShell
import org.appmat.polyglot.notebook.kernel.interpreters.python.PythonJepShell
import org.appmat.polyglot.notebook.kernel.interpreters.scala.ScalaFlinkShell
import org.appmat.polyglot.notebook.kernel.interpolators.*

val mapOfShells = mapOf(
    "kotlin" to KotlinShell(),
    "java" to JavaJShell(),
    "python" to PythonJepShell(),
    "scala" to ScalaFlinkShell(),
    "javascript" to JavascriptShell()
)

val mapOfInterpolators = mapOf(
    "kotlin" to KotlinContextInterpolator(),
    "java" to JavaContextInterpolator(),
    "python" to PythonContextInterpolator(),
    "scala" to ScalaContextInterpolator(),
    "javascript" to JavascriptContextInterpolator()
)