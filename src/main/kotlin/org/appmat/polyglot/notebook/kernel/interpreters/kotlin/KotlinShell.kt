package org.appmat.polyglot.notebook.kernel.interpreters.kotlin

import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.PolyglotType
import org.appmat.polyglot.notebook.kernel.PolyglotVariable
import org.appmat.polyglot.notebook.kernel.interpreters.Interpreters
import org.appmat.polyglot.notebook.kernel.interpreters.ShellConfiguration
import org.appmat.polyglot.notebook.kernel.interpreters.parseSimpleType
import org.jetbrains.kotlinx.ki.shell.KotlinShell
import org.jetbrains.kotlinx.ki.shell.Shell
import org.jetbrains.kotlinx.ki.shell.configuration.CachedInstance
import org.jetbrains.kotlinx.ki.shell.configuration.ReplConfiguration
import org.jetbrains.kotlinx.ki.shell.configuration.ReplConfigurationBase
import java.lang.reflect.Field
import java.util.regex.Pattern
import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties
import kotlin.script.experimental.api.*
import kotlin.script.experimental.jvm.*
import kotlin.script.experimental.jvm.util.SnippetsHistory
import kotlin.script.experimental.util.LinkedSnippetImpl

class KotlinShell() : Interpreters {

    private lateinit var variables: ArrayList<PolyglotVariable>
    private lateinit var constants: ArrayList<PolyglotVariable>
    private lateinit var repl: Shell

    override val language: String
        get() = "kotlin"

    private fun configuration(): ReplConfiguration {
        val instance = CachedInstance<ReplConfiguration>()
        val klassName: String? = System.getProperty("config.class")
        return if (klassName != null) {
            instance.load(klassName, ReplConfiguration::class)
        } else {
            instance.get { object : ReplConfigurationBase() {} }
        }
    }

    private fun createRepl(): Shell {
        return Shell(
            configuration(),
            defaultJvmScriptingHostConfiguration,
            ScriptCompilationConfiguration {
                jvm {
                    dependenciesFromClassloader(
                        classLoader = KotlinShell::class.java.classLoader,
                        wholeClasspath = true
                    )
                }
            },
            ScriptEvaluationConfiguration {
                jvm {
                    baseClassLoader(Shell::class.java.classLoader)
                }
            }
        )
    }

    override fun initShell(config: ShellConfiguration?) {
        repl = createRepl()
        repl.settings = Shell.Settings(configuration())
    }

    sealed class EvalResult(val name: String, val typeName: String? = null) {
        class Unit(name: String): EvalResult(name, "Unit")
        class Value(name: String, typename: String? = null, value: String? = null): EvalResult(name, typename) {
            var type: Class<*>? = try {
                Class.forName(typename)
            } catch (_: Exception) {
                null
            }
            val value: Any? = if(value != null && type == null) {
                val result = when (typename?.replace("kotlin.", "")) {
                    "Int" -> value.toInt()
                    "Byte" -> value.toByte()
                    "Long" -> value.toLong()
                    "Float" -> value.toFloat()
                    "Short" -> value.toShort()
                    "Double" -> value.toDouble()
                    "Boolean" -> value.toBoolean()
                    else -> value
                }
                type = result.javaClass
                result
            }
            else null
        }
    }

    private fun getEvalResult(result: ResultWithDiagnostics<*>): EvalResult? {
        val resultOrNull = result.valueOrNull()

        return if (resultOrNull != null) {
            val resolved = ((resultOrNull.asSuccess().value as LinkedSnippetImpl<*>).get() as KJvmEvaluatedSnippet).result
            val rawResult = resolved.toString().split(" = ", limit = 2)
            val rawType = rawResult[0].split(' ', limit = 2)

            if (rawResult.size > 1) {
                EvalResult.Value(rawType[0], rawType[1], rawResult[1])
            } else EvalResult.Unit(rawType[0])
        } else null
    }

    override fun getVariables(): PolyglotContext {
        variables = arrayListOf<PolyglotVariable>()
        constants = arrayListOf<PolyglotVariable>()

        var field: Field = Shell::class.java.getDeclaredField("evaluator")
        field.isAccessible = true
        val evaluator = field.get(repl)
        field = BasicJvmReplEvaluator::class.java.getDeclaredField("history")
        field.isAccessible = true

        var history = field.get(evaluator)
        var historyItems = ((history as SnippetsHistory<*,*>).items as java.util.ArrayList<*>)
        val mapOfName: MutableMap<String, Boolean> = mutableMapOf()

        for (item in historyItems) {
            var pair = item as Pair<*, *>
            var memberProps = ((pair.first as KClass<*>).memberProperties as ArrayList)
            if (memberProps.isEmpty()) {
                continue
            }
            var member = memberProps[0]
            val name = member.name
            val isConst = (member.getter.property as Any).toString().split(" ")[0] == "val"
            mapOfName[name] = isConst
        }

        mapOfName.forEach { (name, isConst) ->
            val res = getEvalResult(repl.eval(name).result) as EvalResult.Value
            val rawType = res.typeName.toString()
            val value = parseValue(res.value.toString(), rawType)
            val type: PolyglotType? = parseVariableType(rawType)
            val meta = if (isConst) "const" else null

            if (meta.equals("const")) {
                constants.add(PolyglotVariable(name = name, value = value, type = rawType, polyglotType = type, meta = meta))
            } else {
                variables.add(PolyglotVariable(name = name, value = value, type = rawType, polyglotType = type, meta = meta))
            }
        }
        return PolyglotContext(variables, constants, null)
    }

    override fun parseVariableType(type: String): PolyglotType? {
        with(type) {
            val simpleType = split(".")[1]
            val isSimpleType: Boolean = split(".").size == 2
            if (isSimpleType) {
                return parseSimpleType(simpleType)
            } else {
                var complexType = split("collections.")[1]

                val complexMatcherMap = Pattern.compile("<\\w+.(?<key>\\w+), \\w+.(?<value>\\w+)>").matcher(this)
                val complexMatcherList = Pattern.compile("<\\w+.(?<key>\\w+)>").matcher(this)
                try {
                    complexType = complexType.substringBefore("/*").trimEnd().split("<")[0]
                } catch (ex: Exception) {
                    println(ex.message)
                }
                val t: PolyglotType? = when (complexType) {
                    "Map" -> {
                        complexMatcherMap.find()
                        val key = parseSimpleType(complexMatcherMap.group("key"))
                        val value = parseSimpleType(complexMatcherMap.group("value"))
                        PolyglotType.ComplexType.MapType(key = key, value = value)
                    }
                    "ArrayList" -> {
                        complexMatcherList.find()
                        val key: PolyglotType.SimpleType? = parseSimpleType(complexMatcherList.group("key"))
                        PolyglotType.ComplexType.ListType(type = key)
                    }
                    "Set" -> {
                        complexMatcherList.find()
                        val key: PolyglotType.SimpleType? = parseSimpleType(complexMatcherList.group("key"))
                        PolyglotType.ComplexType.SetType(type = key)
                    }
                    else -> null
                }
                return t
            }
        }
    }

    private fun parseValue(value: String, rawType: String) : String {
        var returnValue = value
        if (value.contains("java.util.ArrayList")) {
            returnValue = value.split(" = ")[1]
        }
        if (rawType.contains("kotlin.collections.Set")) {
            returnValue = value
                .replace("[", "{")
                .replace("]", "}")
        }
        return returnValue
    }

    override fun execute(code: String) : Unit {
        try {
            code.split("\n").forEach { it ->
                repl.eval(it)
            }
        } catch (ex: Exception) {
            println("ex: ${ex.message}")
        }
    }

    override fun reset() {
    }

    override fun shutdown() {

    }

}