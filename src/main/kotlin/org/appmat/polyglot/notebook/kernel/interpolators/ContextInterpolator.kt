package org.appmat.polyglot.notebook.kernel.interpolators

import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.PolyglotType
import org.appmat.polyglot.notebook.kernel.unionVariables

interface ContextInterpolate {
    fun generateInterpolatedCode(contextVariables: PolyglotContext) : String
}

fun ContextInterpolate.parseSimpleType(simpleType: String): PolyglotType.SimpleType? {
    val type = when (simpleType) {
        "Int", "int", "Integer" -> PolyglotType.SimpleType.IntegerType()
        "String", "string" -> PolyglotType.SimpleType.StringType()
        "Double", "double" -> PolyglotType.SimpleType.DoubleType()
        "Long", "long" -> PolyglotType.SimpleType.LongType()
        "Boolean", "boolean", "bool" -> PolyglotType.SimpleType.BooleanType()
        else -> null
    }
    return type
}

fun ContextInterpolate.parseComplexType(complexType: String): PolyglotType.ComplexType? {
    val type = when (complexType) {
        "Map", "map" -> PolyglotType.ComplexType.MapType()
        "Set", "set" -> PolyglotType.ComplexType.SetType()
        "List", "list" -> PolyglotType.ComplexType.ListType()
        else -> null
    }
    return type
}


class PythonContextInterpolator : ContextInterpolate {
    override fun generateInterpolatedCode(contextVariables: PolyglotContext) : String {
        val builder = StringBuilder()
        val unionVars = unionVariables(unionVariables(contextVariables.variables, contextVariables.constants), contextVariables.objects)
        for (variable in unionVars) {
            val name = variable.name
            val type = variable.polyglotType
            val value = variable.value

            val variableMember = when (type) {
                is PolyglotType.SimpleType.IntegerType,
                is PolyglotType.SimpleType.DoubleType,
                is PolyglotType.SimpleType.LongType -> {
                    "$name = $value\n"
                }
                is PolyglotType.SimpleType.StringType -> {
                    if (value.toString().count { it == '\"' } == 2)
                        "$name = $value\n"
                    else
                        "$name = \"$value\"\n"
                }
                is PolyglotType.SimpleType.BooleanType -> {
                    "$name = ${value.toString().capitalize()}\n"
                }

                is PolyglotType.ComplexType.ListType -> {
                    val simpleKey = parseSimpleType(type.type?.name ?: "")
                    val listArg = when (simpleKey) {
                        is PolyglotType.SimpleType.StringType -> {
                            value.toString()
                                .replace("[", "[\"")
                                .replace("]", "\"]")
                                .replace(", ", "\", \"")
                        }
                        else -> {
                            value
                        }
                    }
                    "$name = $listArg\n"
                }
                is PolyglotType.ComplexType.SetType -> {
                    val simpleKey = parseSimpleType(type.type?.name ?: "")

                    val set = when (simpleKey) {
                        is PolyglotType.SimpleType.IntegerType,
                        is PolyglotType.SimpleType.LongType -> {
                            value.toString()
                                .replace("[", "{")
                                .replace("]", "}")
                        }
                        is PolyglotType.SimpleType.StringType -> {
                            value.toString()
                                .replace("[", "{\"")
                                .replace("]", "\"}")
                                .replace(", ", "\", \"")
                        }
                        else -> "\n"
                    }
                    "$name = $set\n"
                }
                is PolyglotType.ComplexType.MapType -> {
                    val simpleKey = parseSimpleType(type.key?.name ?: "")
                    var keyToValue: String = ""
                    keyToValue = when (simpleKey) {
                        is PolyglotType.SimpleType.StringType -> {
                            (value as String)
                                .replace("{", "{\"")
                                .replace("=", "\": ")
                                .replace(", ", ", \"")
                        }
                        is PolyglotType.SimpleType.IntegerType,
                        is PolyglotType.SimpleType.LongType,
                        is PolyglotType.SimpleType.DoubleType,
                        is PolyglotType.SimpleType.BooleanType -> {
                            (value as String)
                                .replace("=", ": ")
                        }
                        null -> {
                            "\n"
                        }
                    }

                    val simpleValue = parseSimpleType(type.value?.name ?: "")
                    if (simpleValue is PolyglotType.SimpleType.StringType) {
                        keyToValue =
                            keyToValue
                                .replace(": ", ": \"")
                                .replace(",", "\",")
                                .replace("}", "\"}")
                    }

                    "$name = $keyToValue\n"
                }
                else -> ""
            }
            builder.append(variableMember)
        }
        return builder.toString()
    }
}

class ScalaContextInterpolator : ContextInterpolate {
    override fun generateInterpolatedCode(contextVariables: PolyglotContext) : String {
        val builder = StringBuilder()
        val unionVars = unionVariables(unionVariables(contextVariables.variables, contextVariables.constants), contextVariables.objects)
        for (variable in unionVars) {
            val name = variable.name
            val type = variable.polyglotType
            val value = variable.value
            val meta = variable.meta
            var id = "var"
            if (meta.equals("const")) id = "val"

            var variableMember = when (type) {
                is PolyglotType.SimpleType.IntegerType -> "var $name: Int = $value\n"
                    is PolyglotType.SimpleType.LongType -> "var $name: Long = $value\n"
                    is PolyglotType.SimpleType.DoubleType -> "var $name: Double = $value\n"
                    is PolyglotType.SimpleType.BooleanType -> "var $name: Boolean = $value\n"
                    is PolyglotType.SimpleType.StringType -> {
                        if (value.toString().count { it == '\"' } == 2)
                            "var $name = $value\n"
                        else
                            "var $name = \"$value\"\n"
                    }
                    else -> null
            }
            if (variableMember == null) {
                when (type) {
                    is PolyglotType.ComplexType.ListType -> {
                        val simpleType = parseSimpleType(type.type?.name ?: "")
                        if (simpleType != null) {
                            when (simpleType) {
                                is PolyglotType.SimpleType.IntegerType -> {
                                    val listIntValues = value.toString()
                                        .replace("[", "")
                                        .replace("]", "")
                                    variableMember = "$id $name: List[Int] = List($listIntValues)\n"
                                }
                                is PolyglotType.SimpleType.LongType -> {
                                    val listLongValues = value.toString()
                                        .replace("[", "")
                                        .replace("]", "")

                                    variableMember = "$id $name: List[Long] = List($listLongValues)\n"
                                }
                                is PolyglotType.SimpleType.StringType -> {
                                    val listStrValues = value.toString()
                                        .replace("[", "\"")
                                        .replace("]", "\"")
                                        .replace(", ", "\", \"")

                                    variableMember = "$id $name: List[String] = List($listStrValues)\n"
                                }
                                else -> {
                                    variableMember = "\n"
                                }
                            }
                        } else {
                            variableMember = "\n"
                        }
                    }
                    is PolyglotType.ComplexType.SetType -> {
                        val simpleType = parseSimpleType(type.type?.name ?: "")
                        if (simpleType != null) {
                            when (simpleType) {
                                is PolyglotType.SimpleType.IntegerType,
                                is PolyglotType.SimpleType.LongType -> {
                                    var setValues = ""
                                    if (value.toString().contains("[\\[\\]]".toRegex())) {
                                        setValues = value.toString()
                                            .replace("[", "")
                                            .replace("]", "")
                                    } else {
                                        setValues = value.toString()
                                            .replace("{", "")
                                            .replace("}", "")
                                    }
                                    variableMember = "$id $name = Set($setValues)\n"
                                }
                                is PolyglotType.SimpleType.StringType -> {
                                    var rawValue = value.toString()
                                    var setStrValues = ""
                                    var rawValueWithoutCurlyBrackets = value.toString().substring(1, rawValue.length-1)

                                    if (rawValueWithoutCurlyBrackets.first() == '\'' && rawValueWithoutCurlyBrackets.last() == '\'') {
                                        rawValue = rawValue.replace("\'", "")
                                    }
                                    if (value.toString().contains("[\\[\\]]".toRegex())) {
                                        setStrValues = rawValue
                                            .replace("[", "\"")
                                            .replace("]", "\"")
                                            .replace(", ", "\", \"")
                                    } else {
                                        setStrValues = rawValue
                                            .replace("{", "\"")
                                            .replace("}", "\"")
                                            .replace(", ", "\", \"")
                                    }
                                    variableMember = "$id $name = Set($setStrValues)\n"
                                }
                                else -> {
                                    variableMember = "\n"
                                }
                            }
                        } else {
                            variableMember = "\n"
                        }
                    }
                    is PolyglotType.ComplexType.MapType -> {
                        var k = ""
                        var v = ""
                        val simpleKey = parseSimpleType(type.key?.name ?: "")
                        var keyToValue: String = ""
                        keyToValue = when (simpleKey) {
                            is PolyglotType.SimpleType.StringType -> {
                                k = "String"
                                (value as String)
                                    .replace("{", "\"")
                                    .replace("}", "")
                                    .replace("=", "\" -> ")
                                    .replace(", ", ", \"")
                            }
                            is PolyglotType.SimpleType.IntegerType,
                            is PolyglotType.SimpleType.LongType -> {
                                k = "Int"
                                (value as String)
                                    .replace("[{}]".toRegex(), "")
                                    .replace("=", " -> ")
                            }
                            else -> {
                                k = "Undefined"
                                ""
                            }
                        }

                        when (parseSimpleType(type.value?.name ?: "")) {
                            is PolyglotType.SimpleType.IntegerType,
                            is PolyglotType.SimpleType.LongType -> {
                                v = "Int"
                                keyToValue = keyToValue
                                    .replace(" -> \"", " -> ")
                                    .replace("\", ", ", ")
                            }
                            is PolyglotType.SimpleType.StringType -> {
                                v = "String"
                                keyToValue = keyToValue
                                    .replace(" -> ", " -> \"")
                                    .replace(", ", "\", ")
                                    .plus("\"")
                            }
                            else -> {}
                        }
                        variableMember = "$id $name: Map[$k, $v] = Map.apply($keyToValue)\n"
                    }
                    else -> {
                        variableMember = "\n"
                    }
                }
            }
            builder.append(variableMember)
        }
        return builder.toString()
    }
}

class KotlinContextInterpolator : ContextInterpolate {
    override fun generateInterpolatedCode(contextVariables: PolyglotContext) : String {
        val builder = StringBuilder()
        val unionVars = unionVariables(unionVariables(contextVariables.variables, contextVariables.constants), contextVariables.objects)
        var id = "var"
        for (variable in unionVars) {
            val name = variable.name
            val type = variable.polyglotType
            var value = variable.value
            val meta = variable.meta

            if (meta.equals("const")) id = "val"

            var variableMember = when (type) {
                is PolyglotType.SimpleType.IntegerType -> "$id $name: Int = $value\n"
                is PolyglotType.SimpleType.DoubleType -> "$id $name: Double = $value\n"
                is PolyglotType.SimpleType.BooleanType -> "$id $name: Boolean = $value\n"
                is PolyglotType.SimpleType.StringType -> {
                    if (value.toString().filter { it == '\"' }.count() == 2)
                        "$id $name: String = $value\n"
                    else
                        "$id $name: String = \"$value\"\n"
                }
                is PolyglotType.SimpleType.LongType -> "$id $name: Long = $value\n"
                else -> null
            }
            if (variableMember == null) {
                when (type) {
                    is PolyglotType.ComplexType.ListType -> {
                        val simpleType = parseSimpleType(type.type?.name ?: "")
                        variableMember = when (simpleType) {
                            is PolyglotType.SimpleType.StringType -> {
                                val listValue = (value as String)
                                    .replace("[\\[\\]]".toRegex(), "\"")
                                    .replace(", ", "\", \"")
                                "$id $name = listOf($listValue)\n"
                            }
                            else -> {
                                try {
                                    value = (value as String)
                                } catch (ex: Exception) {
                                    value = (value as ArrayList<*>).toString()
                                }
                                val listValue = value.toString()
                                    .replace("[\\[\\]]".toRegex(), "")
                                "$id $name = listOf($listValue)\n"
                            }
                        }
                    }
                    is PolyglotType.ComplexType.MapType -> {
                        val simpleKey = parseSimpleType(type.key?.name ?: "")
                        var keyToValue: String = ""
                        keyToValue = when (simpleKey) {
                            is PolyglotType.SimpleType.StringType -> {
                                (value as String)
                                    .replace("[{}]".toRegex(), "\"")
                                    .replace("=", "\" to \"")
                                    .replace(", ", "\", \"")
                            }
                            is PolyglotType.SimpleType.IntegerType,
                            is PolyglotType.SimpleType.LongType,
                            is PolyglotType.SimpleType.DoubleType,
                            is PolyglotType.SimpleType.BooleanType -> {
                                (value as String)
                                    .replace("=", " to ")
                                    .replace("[{]".toRegex(), "")
                                    .replace("}", "\"")
                                    .replace("to ", "to \"")
                                    .replace(", ", "\", ")
                            }
                            null -> {
                                try {
                                    (value as HashMap<*,*>).toString()
                                        .replace("[{}]".toRegex(), "\"")
                                        .replace("=", "\" to \"")
                                        .replace(", ", "\", \"")
                                } catch (ex: Exception) {
                                    (value as String).toString()
                                        .replace("[{}]".toRegex(), "\"")
                                        .replace("=", "\" to \"")
                                        .replace(", ", "\", \"")
                                }
                            }
                        }
                        val simpleValue = parseSimpleType(type.value?.name ?: "")
                        if (simpleValue is PolyglotType.SimpleType.IntegerType) {
                            keyToValue =
                                keyToValue
                                    .replace("to \"", "to ")
                                    .replace("\", ", ", ")
                                    .dropLast(1)
                        }

                        variableMember = "$id $name = mapOf($keyToValue)\n"
                    }
                    is PolyglotType.ComplexType.SetType -> {
                        val simpleType = parseSimpleType(type.type?.name ?: "")
                        variableMember = if (simpleType != null) {
                            when (simpleType) {
                                is PolyglotType.SimpleType.IntegerType -> {
                                    val setValue = value.toString()
                                        .replace("[\\[\\]]".toRegex(), "")
                                        .replace("{", "")
                                        .replace("}", "")
                                    "$id $name = setOf($setValue)\n"
                                }
                                is PolyglotType.SimpleType.StringType -> {
                                    val raw = value.toString()
                                    var setValue = raw.substring(1, raw.length-1)
                                    if (setValue.first() == '\'' && setValue.last() == '\'') {
                                        setValue = setValue.replace("\'", "\"")
                                    }
                                    "$id $name = setOf($setValue)\n"
                                }
                                else -> {
                                    "\n"
                                }
                            }
                        } else {
                            "\n"
                        }
                    }

                    else -> {
                        variableMember = "\n"
                    }
                }
            }
            builder.append(variableMember)
        }
        return builder.toString()
    }
}

class JavaContextInterpolator : ContextInterpolate {
    override fun generateInterpolatedCode(contextVariables: PolyglotContext) : String {
        val builder = StringBuilder()
        val unionVars = unionVariables(unionVariables(contextVariables.variables, contextVariables.constants), contextVariables.objects)
        for (variable in unionVars) {
            val name = variable.name
            val type = variable.polyglotType
            val value = variable.value

            var variableMember = when (type) {
                is PolyglotType.SimpleType.IntegerType -> "int $name = $value;\n"
                is PolyglotType.SimpleType.DoubleType -> "double $name = $value;\n"
                is PolyglotType.SimpleType.BooleanType -> "Boolean $name = $value;\n"
                is PolyglotType.SimpleType.StringType -> {
                    if (value.toString().filter { it == '\"' }.count() == 2)
                        "String $name = $value;\n"
                    else
                        "String $name = \"$value\";\n"
                }
                is PolyglotType.SimpleType.LongType -> "long $name = $value;\n"
                else -> null
            }
            if (variableMember == null) {
                var listPutter: String = ""
                var listType = "String"
                var setPutter: String = ""
                var setType = "String"
                when (type) {
                    is PolyglotType.ComplexType.ListType -> {
                        val simpleType = parseSimpleType(type.type?.name ?: "")
                        if (simpleType != null) {
                            val rawValues = (value as String)
                                .replace("[\\[\\]]".toRegex(), "")
                                .filter { !it.isWhitespace() }
                                .split(",")
                            when (simpleType) {
                                is PolyglotType.SimpleType.StringType -> {
                                    rawValues.forEach { v ->
                                        listPutter += "$name.add(\"$v\");\n"
                                    }
                                }
                                is PolyglotType.SimpleType.IntegerType,
                                is PolyglotType.SimpleType.LongType -> {
                                    rawValues.forEach { v ->
                                        listPutter += "$name.add($v);\n"
                                    }
                                    listType = "Integer"
                                }
                                else -> {
                                    listPutter = ""
                                }
                            }
                        } else {
                            (value as ArrayList<*>).forEach {
                               listPutter += "$name.add($it);\n"
                            }
                        }
                        variableMember = "ArrayList<$listType> $name = new ArrayList<$listType>();\n"
                        variableMember += listPutter

                    }
                    is PolyglotType.ComplexType.MapType -> {
                        val simpleKey = parseSimpleType(type.key?.name ?: "")
                        val pattern = "(\\w+)".toRegex()
                        var mapPutter: String = ""
                        var resKey: String = "String"
                        var resValue: String = "String"
                        val simpleValue = parseSimpleType(type.value?.name ?: "")
                        val isValueString: Boolean = simpleValue is PolyglotType.SimpleType.StringType
                        if (!isValueString)
                            resValue = "Integer"

                        when (simpleKey) {
                            is PolyglotType.SimpleType.StringType -> {
                                var i = 0
                                var k: String = ""
                                var v: String = ""
                                pattern.findAll(value as String).forEach {
                                    if (i % 2 == 0) {
                                        k = "\"${it.value}\""
                                    } else {
                                        v = if (isValueString)
                                            "\"${it.value}\""
                                        else
                                            it.value
                                        mapPutter += "$name.put($k, $v);\n"
                                    }
                                    i++
                                }
                            }
                            is PolyglotType.SimpleType.IntegerType,
                            is PolyglotType.SimpleType.LongType -> {
                                resKey = "Integer"
                                var i = 0
                                var k: Int = 0
                                var v: String = ""
                                pattern.findAll(value as String).forEach {
                                    if (i % 2 == 0) {
                                        k = it.value.toInt()
                                    } else {
                                        v = if (isValueString)
                                            "\"${it.value}\""
                                        else
                                            it.value
                                        mapPutter += "$name.put($k, $v);\n"
                                    }
                                    i++
                                }
                            }
                            else -> {
                                (value as HashMap<*,*>).forEach {
                                    mapPutter += "$name.put(${it.key}, ${it.value});\n"
                                }
                            }
                        }
                        variableMember = "HashMap<$resKey, $resValue> $name = new HashMap<>();\n";
                        variableMember += mapPutter
                    }
                    is PolyglotType.ComplexType.SetType -> {
                        val simpleType = parseSimpleType(type.type?.name ?: "")
                        var setValue = value.toString()
                        if (setValue.first() == '{' && setValue.last() == '}') {
                            setValue = setValue
                                .replace("{", "")
                                .replace("}", "")
                        }
                        if (setValue.first() == '\'' && setValue.last() == '\'') {
                            setValue = setValue.replace("\'", "")
                        }
                        val rawValues = setValue
                            .replace("[\\[\\]]".toRegex(), "")
                            .filter { !it.isWhitespace() }
                            .split(",")

                        if (simpleType != null) {
                            when (simpleType) {
                                is PolyglotType.SimpleType.StringType -> {
                                    rawValues.forEach { v ->
                                        v.replace("'", "\"")
                                        setPutter += "$name.add(\"$v\");\n"
                                    }
                                }
                                is PolyglotType.SimpleType.IntegerType -> {
                                    rawValues.forEach { v ->
                                        setPutter += "$name.add($v);\n"
                                    }
                                    setType = "Integer"
                                }
                                else -> {
                                    setPutter = ""
                                }
                            }
                        } else {
                            (setValue as HashSet<*>).forEach {
                                listPutter += "$name.add($it);\n"
                            }
                        }
                        variableMember = "HashSet<$setType> $name = new HashSet<$setType>();\n"
                        variableMember += setPutter
                    }
                    // PyObject
                    else -> {
                        variableMember = ""
                    }
                }
            }
            builder.append(variableMember)
            builder.append("\n")
        }
        return builder.toString()
    }
}

class JavascriptContextInterpolator : ContextInterpolate {
    override fun generateInterpolatedCode(contextVariables: PolyglotContext): String {

        val builder = StringBuilder()
        val unionVars = unionVariables(unionVariables(contextVariables.variables, contextVariables.constants), contextVariables.objects)
        for (variable in unionVars) {
            val name = variable.name
            val type = variable.polyglotType
            val value = variable.value
            val meta = variable.meta
            var id = if (meta == "const") "const" else "var"

            val variableMember = when (type) {
                is PolyglotType.SimpleType.IntegerType,
                is PolyglotType.SimpleType.LongType,
                is PolyglotType.SimpleType.BooleanType,
                is PolyglotType.SimpleType.DoubleType -> {
                    "$id $name = $value;\n"
                }
                is PolyglotType.SimpleType.StringType -> {
                    if (value.toString().filter { it == '\"' }.count() == 2)
                        "$id $name = $value;\n"
                    else
                        "$id $name = \"$value\";\n"
                }
                is PolyglotType.ComplexType.ListType -> {
                    val simpleType = parseSimpleType(type.type?.name ?: "")
                    if (simpleType != null) {
                        when (simpleType) {
                            is PolyglotType.SimpleType.IntegerType,
                            is PolyglotType.SimpleType.LongType -> {
                                "$id $name = $value;\n"
                            }
                            is PolyglotType.SimpleType.StringType -> {
                                val rawValues = (value as String)
                                    .replace("[", "[\"")
                                    .replace("]", "\"]")
                                    .replace(", ", "\", \"")
                                "$id $name = $rawValues;\n"
                            }
                            else -> "\n"
                        }
                    } else {
                        "\n"
                    }
                }
                is PolyglotType.ComplexType.MapType -> {
                    val simpleKey = parseSimpleType(type.key?.name ?: "")
                    var keyToValue: String = ""
                    keyToValue = when (simpleKey) {
                        is PolyglotType.SimpleType.StringType -> {
                            value.toString()
                                .replace("{", "{\"")
                                .replace("=", "\": ")
                                .replace(", ", ", \"")
                        }
                        is PolyglotType.SimpleType.IntegerType,
                        is PolyglotType.SimpleType.LongType,
                        is PolyglotType.SimpleType.DoubleType,
                        is PolyglotType.SimpleType.BooleanType -> {
                            value.toString()
                                .replace("=", ": ")
                        }
                        null -> {
                            "\n"
                        }
                    }

                    val simpleValue = parseSimpleType(type.value?.name ?: "")
                    if (simpleValue is PolyglotType.SimpleType.StringType) {
                        keyToValue =
                            keyToValue
                                .replace(": ", ": \"")
                                .replace(",", "\",")
                                .replace("}", "\"}")
                    }
                    "$id $name = $keyToValue;\n"
                }
                is PolyglotType.ComplexType.SetType -> {
                    val simpleType = parseSimpleType(type.type?.name ?: "")
                    var setValue = ""
                    if (simpleType != null) {
                        when (simpleType) {
                            is PolyglotType.SimpleType.IntegerType,
                            is PolyglotType.SimpleType.DoubleType -> {
                                val setValue = value.toString()
                                    .replace("{", "")
                                    .replace("}", "")
                                "$id $name = new Set([$setValue]);\n"
                            }
                            is PolyglotType.SimpleType.StringType -> {
                                setValue = value.toString()
                                    .replace("{", "")
                                    .replace("}", "")

                                if (!setValue.contains("\"") && !setValue.contains("\'")) {
                                    setValue = "\"$setValue\""
                                    setValue.replace(", ", "\", \"")
                                }
                                "$id $name = new Set([$setValue]);\n"
                            }
                            else -> "\n"
                        }
                    } else {
                        "\n"
                    }
                }
                else -> "\n"
            }
            builder.append(variableMember)
        }
        return builder.toString()
    }

}

class DefaultContextInterpolate : ContextInterpolate {
    override fun generateInterpolatedCode(contextVariables: PolyglotContext): String {
        TODO("Not yet implemented")
    }
}

val defaultContextInterpolator = DefaultContextInterpolate()