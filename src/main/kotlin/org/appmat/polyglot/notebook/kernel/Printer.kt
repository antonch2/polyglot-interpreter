package org.appmat.polyglot.notebook.kernel

class Printer() {

    companion object {
        fun prettify(shell: String = "", ctx: PolyglotContext) {
            println(if (shell != "") "`$shell` ctx: ".trimIndent() else "ctx: ")
            val allVars = unionVariables(unionVariables(ctx.variables, ctx.constants), ctx.objects)
            for (variable in allVars) {
                println(
                    "name = ${variable.name} \n" +
                            "value = ${variable.value} \n" +
                            "type = ${variable.type} \n" +
                            "polyglotType = ${variable.polyglotType} \n" +
                            "meta = ${variable.meta}"
                )
                println("--------- `$shell` ---------".filter { !it.isWhitespace() })
            }
            println()
        }

        fun raw(shell: String = "", ctx: PolyglotContext) {
            println(if (shell != "") "`$shell` ctx: " else "ctx: ")

            if (ctx == defaultContext) {
                ctx.clear()
                println(ctx)
                return
            } else if ((ctx == emptyContext) || (ctx == emptyContextWithConstNull) ||
                (ctx == emptyContextWithConstNull) || (ctx == emptyContextWithObjectNull) ||
                (ctx == emptyContextWithConstObjectNull))
            {
                println(ctx)
                return
            }
            val allVars = unionVariables(unionVariables(ctx.variables, ctx.constants), ctx.objects)
            for ((i, vars) in allVars.withIndex()) {
                println("$i $vars")
            }
            println()
        }

        fun oneLine(ctx: PolyglotContext, color: String = "\u001B[0m") {
            if (ctx == defaultContext) {
                ctx.clear()
                println(color + "ctx: $ctx")
                return
            } else {
                println(color + "ctx: $ctx")
                return
            }
        }
    }
}