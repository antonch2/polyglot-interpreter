package org.appmat.polyglot.notebook.kernel.interpreters.scala

import org.appmat.polyglot.notebook.kernel.Printer

fun main() {
    val scalaShell = ScalaFlinkShell()
    scalaShell.initShell()

    val ctx = scalaShell.getVariables()
    Printer.raw(shell = "scala", ctx = ctx)
}
