package org.appmat.polyglot.notebook.kernel

data class PolyglotVariable(var name: String, var value: Any?, val polyglotType: PolyglotType? = null, var type: String? = null, val meta: String? = null)

sealed interface PolyglotType {

    sealed class SimpleType(val name: String) : PolyglotType {
        class IntegerType() : SimpleType("int")
        class StringType() : SimpleType("string")
        class DoubleType() : SimpleType("double")
        class LongType() : SimpleType("long")
        class BooleanType() : SimpleType("boolean")
    }

    sealed class ComplexType(val name: String) : PolyglotType {
        data class ListType(val type: SimpleType? = null) : ComplexType("list")
        data class MapType(val key: SimpleType? = null, val value: SimpleType? = null) : ComplexType("map")
        data class SetType(val type: SimpleType? = null) : ComplexType("set")
    }
}

data class PolyglotContext (
    var variables: ArrayList<PolyglotVariable>,
    var constants: ArrayList<PolyglotVariable>? = null,
    var objects: ArrayList<PolyglotVariable>? = null
)

fun unionVariables(one: ArrayList<PolyglotVariable>, two: ArrayList<PolyglotVariable>?) : ArrayList<PolyglotVariable> {
    var newVars: ArrayList<PolyglotVariable> = ArrayList<PolyglotVariable>(one)
    if (two != null) {
        for (vars in two) {
            newVars.add(PolyglotVariable(name = vars.name, value = vars.value, polyglotType = vars.polyglotType, type = vars.type, meta = vars.meta))
        }
    }
    return newVars
}

fun PolyglotContext.clear() {
    this.variables = arrayListOf()
    this.constants = arrayListOf()
    this.objects = arrayListOf()
}

class PolyglotWarning (msg: String): Exception(msg)
class PolyglotException (msg: String): Exception(msg)


fun PolyglotContext.removeDuplicates(newCtx: PolyglotContext) : PolyglotContext {
    val defaultName = this.variables.indexOfFirst { it ->
        it.name == "default"
    }
    val isConstantNull = this.constants == null
    val isObjectNull = this.objects == null
    val isConstEmpty = (if (isConstantNull) false else this.constants!!.isEmpty())
    val isObjectEmpty = (if (isObjectNull) false else this.objects!!.isEmpty())

    if (this.variables.isEmpty()) {
        if (!isConstantNull && !isObjectNull)
            if (isConstEmpty && isObjectEmpty)
                return newCtx
        if (isConstantNull && !isObjectNull)
            if (isObjectEmpty)
                return newCtx
        if (isConstantNull && isObjectNull) {
                return newCtx
        }
    }

    if (defaultName != -1) {
        return newCtx
    }

    var resCtx = PolyglotContext(
        ArrayList<PolyglotVariable>(this.variables),
        ArrayList<PolyglotVariable>(if (this.constants != null) this.constants else arrayListOf()),
        ArrayList<PolyglotVariable>(if (this.objects != null) this.objects else arrayListOf()),
    )
    val allNewVars = unionVariables(unionVariables(newCtx.variables, newCtx.constants), newCtx.objects)
    val allOldVars = unionVariables(unionVariables(this.variables, this.constants), this.objects)

    allNewVars.forEach {
        allOldVars.forEach { old ->
            val newName = it.name
            val newVal = it.value
            val newPolyType = it.polyglotType
            val newConst = it.meta
            val newType = it.type

            if ((old.name == newName)) {
                val foundIndex = this.variables.indexOfFirst { variable -> variable.name == old.name }
                val foundIndexConst = this.constants?.indexOfFirst { const -> const.name == old.name }

                val valueWasChanged = old.value != newVal
                if (old.meta != "const" && foundIndex != -1 && valueWasChanged) {
                    if (newPolyType != null)
                        resCtx.variables[foundIndex].value = newVal
                } else if (old.meta == "const" && foundIndexConst != -1 && valueWasChanged)
                    throw PolyglotException("const $newName was changed !!!")
            } else {
                if (!old.meta.equals("const")) {
                    val isNotFound1 = resCtx.variables.filter { vars -> vars.name == newName }.size
                    val isNotFound2 = resCtx.constants?.filter { vars -> vars.name == newName }?.size
                    val isNotFound3 = resCtx.objects?.filter { vars -> vars.name == newName }?.size

                    if (isNotFound2 != null && isNotFound3 != null) {
                        if (isNotFound1 + isNotFound2 + isNotFound3 == 0) {
                            val isNotFound = isNotFound1 + isNotFound2 + isNotFound3 == 0
                            if (isNotFound && newPolyType == null) {
                                resCtx.objects?.add(PolyglotVariable(newName, newVal, newPolyType, newType, newConst))
                            } else if (isNotFound && !newConst.equals("const")) {
                                resCtx.variables.add(PolyglotVariable(newName, newVal, newPolyType, newType, newConst))
                            } else if (isNotFound && newConst.equals("const")) {
                                resCtx.constants?.add(PolyglotVariable(newName, newVal, newPolyType, newType, newConst))
                            }
                        }
                    }
                } else {
                    val isNotFound1 = resCtx.variables.filter { vars -> vars.name == newName }.size
                    val isNotFound2 = resCtx.constants?.filter { vars -> vars.name == newName }?.size
                    val isNotFound3 = resCtx.objects?.filter { vars -> vars.name == newName }?.size

                    if (isNotFound2 != null) {
                        val isNotFound = isNotFound1 + isNotFound2 == 0
                        if (isNotFound && !newConst.equals("const") && newPolyType != null) {
                            resCtx.variables.add(PolyglotVariable(newName, newVal, newPolyType, newType, newConst))
                        } else if (isNotFound && newConst.equals("const") && newPolyType != null) {
                            resCtx.constants?.add(PolyglotVariable(it.name, it.value, it.polyglotType, it.type, it.meta))
                        } else if (isNotFound) {
                            if (isNotFound3 != null)
                                if (isNotFound3 == 0) {
                                    resCtx.objects?.add(
                                        PolyglotVariable(it.name, it.value, it.polyglotType, it.type, it.meta)
                                    )
                                }
                        }
                    }
                }
            }
        }
    }

    return PolyglotContext(variables = resCtx.variables, constants = resCtx.constants, objects = resCtx.objects)
}


val defaultPolyglotVariable = PolyglotVariable("default", null, null, null, null)
val defaultContext = PolyglotContext(
    arrayListOf(defaultPolyglotVariable),
    arrayListOf(defaultPolyglotVariable),
    arrayListOf(defaultPolyglotVariable))

val emptyContext = PolyglotContext(
    arrayListOf(),
    arrayListOf(),
    arrayListOf()
)
val emptyContextWithConstNull = PolyglotContext(
    arrayListOf(),
    null,
    arrayListOf()
)
val emptyContextWithObjectNull = PolyglotContext(
    arrayListOf(),
    arrayListOf(),
    null
)
val emptyContextWithConstObjectNull = PolyglotContext(
    arrayListOf(),
    null,
    null
)
