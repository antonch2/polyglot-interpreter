package org.appmat.polyglot.notebook.kernel.interpreters

import org.appmat.polyglot.notebook.kernel.PolyglotContext
import org.appmat.polyglot.notebook.kernel.PolyglotType

interface Interpreters {
    val language: String
    fun initShell (config: ShellConfiguration? = null)
    fun getVariables() : PolyglotContext
    fun parseVariableType(type: String): PolyglotType?
    fun execute(code: String) : Unit
    fun reset() : Unit
    fun shutdown() : Unit
}

fun Interpreters.parseSimpleType(simpleType: String): PolyglotType.SimpleType? {
    val type = when (simpleType) {
        "Int", "int", "Integer" -> PolyglotType.SimpleType.IntegerType()
        "String", "string" -> PolyglotType.SimpleType.StringType()
        "Double", "double" -> PolyglotType.SimpleType.DoubleType()
        "Long", "long" -> PolyglotType.SimpleType.LongType()
        "Boolean", "boolean", "bool" -> PolyglotType.SimpleType.BooleanType()
        else -> null
    }
    return type
}

fun Interpreters.parseComplexType(complexType: String): PolyglotType.ComplexType? {
    val type = when (complexType) {
        "Map", "map" -> PolyglotType.ComplexType.MapType()
        "Set", "set" -> PolyglotType.ComplexType.SetType()
        "List", "list" -> PolyglotType.ComplexType.ListType()
        else -> null
    }
    return type
}


class ShellConfiguration {}

val defaultShellConfiguration : ShellConfiguration = ShellConfiguration()

class DefaultShell : Interpreters {
    override val language: String
        get() = "default"

    override fun initShell(config: ShellConfiguration?) {
    }

    override fun getVariables(): PolyglotContext {
        TODO("Not yet implemented")
    }

    override fun parseVariableType(type: String): PolyglotType? {
        TODO("Not yet implemented")
    }

    override fun execute(code: String) {
    }

    override fun reset() {
    }

    override fun shutdown() {
    }

}

val defaultShell = DefaultShell()