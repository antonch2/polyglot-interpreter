package org.appmat.polyglot.notebook.kernel.interpreters.kotlin

import org.appmat.polyglot.notebook.kernel.Printer

fun main() {
    val kishell = KotlinShell()
    kishell.initShell()

    val ctx = kishell.getVariables()
    Printer.raw(shell = kishell.language, ctx = ctx)
}