package org.appmat.polyglot

import io.ktor.server.testing.*

fun <R> withAppIntegrationTest(test: TestApplicationEngine.() -> R) =
    withApplication(createTestEnvironment {
        module {
            module(testing = false)
        }
    }) {
        test()
    }
