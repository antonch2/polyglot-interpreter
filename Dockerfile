FROM gradle:jdk11 as builder
WORKDIR /app
COPY build/libs/polyglot-app.jar /app/polyglot-app.jar
COPY build/libs/META-INF/MANIFEST.MF /app/META-INF/MANIFEST.MF

ENV PORT 5000
EXPOSE 5000
CMD ["java", "-jar", "/app/polyglot-app.jar"]