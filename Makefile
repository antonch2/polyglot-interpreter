.PHONY: build

CONTAINER_NAME = scala-shell
NETWORK = flink-network
FLINK_PROPERTIES = "jobmanager.rpc.address: jobmanager"

build:
	docker pull flink:scala_2.12
	docker network create flink-network >/dev/null 2>&1 || true
	docker run --rm --name=$(CONTAINER_NAME) --network flink-network --publish 8082:8082 --env FLINK_PROPERTIES=$(FLINK_PROPERTIES) flink:scala_2.12 jobmanager

stop:
	docker stop $(CONTAINER_NAME)

start:
	docker start $(CONTAINER_NAME)

remove-cont:
	docker rm $(CONTAINER_NAME)

delete-network:
	docker network rm $(NETWORK)

.DEFAULT_GOAL := build